import cv2
import numpy as np
import time

img = cv2.imread('Parrots.jpg')

height, width, depth = img.shape

print "Normalizing each pixel..."
for i in range (0, height):
	for j in range (0, width):
		#print "i: ", i, "j: ", j
		current_pixel = img[i,j]

		# normalizing

		blue  = img[i, j, 0]
		green = img[i, j, 1]
		red   = img[i, j, 2]
		
		total = float(blue) + float(green) + float(red)

		if total == 0:
			img[i,j] = [0, 0, 0]
		else:
			blue_norm = float(blue) / total * 255
			green_norm = float(green) / total * 255
			red_norm = float(red) / total * 255
			img[i,j] = [blue_norm, green_norm, red_norm]	

print "Done"
img2 = img
cv2.imwrite('normalized.jpg', img2)
cv2.imshow('img', img) 

cv2.waitKey(0)
cv2.destroyAllWindows()


