import cv2
import numpy as np

'''
print "Supported colors are blue, yellow, orange, and red"
color = raw_input("Enter a color: ")

if color == "blue":
	lower_color = np.array([100, 50, 10], dtype=np.uint8)
	upper_color = np.array([120, 255, 255], dtype=np.uint8)
   #blue = cv2.inRange(hsv,np.array((100,100,100)),np.array((120,255,255)))

if color == "yellow":
	lower_color = np.array([30, 25, 10], dtype=np.uint8)
	upper_color = np.array([60, 255, 255], dtype=np.uint8)

# Yellow
if color == "orange":
	lower_color = np.array([20, 20, 100], dtype=np.uint8)
	upper_color = np.array([30, 255, 255], dtype=np.uint8)	
	
if color == "red":
	lower_color = np.array([152, 105, 127], dtype=np.uint8)
	upper_color = np.array([180, 255, 255], dtype=np.uint8)	
'''

cap = cv2.VideoCapture(0)

while (1):

	_, frame = cap.read()
	height, width, depth = frame.shape
	for i in range (0, height):
		for j in range (0, width):
      	#print "i: ", i, "j: ", j
			current_pixel = frame[i,j]

      	# normalizing
			blue  = frame[i, j, 0]
			green = frame[i, j, 1]
			red   = frame[i, j, 2]

			total = float(blue) + float(green) + float(red)
	
			if total == 0:
				frame[i,j] = [0, 0, 0]
			else:
				blue_norm = float(blue) / total * 255
				green_norm = float(green) / total * 255
				red_norm = float(red) / total * 255
				frame[i,j] = [blue_norm, green_norm, red_norm]

	cv2.imshow('frame',frame)

	k = cv2.waitKey(5) & 0xFF
	if k == 27:
		break

cv2.destroyAllWindows()
